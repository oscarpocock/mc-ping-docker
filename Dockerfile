FROM rust:alpine AS builder
ENV COMMAND=~ping \
    TOKEN=token \
    ADDRESS=localhost
RUN apk -U --no-cache upgrade
RUN apk add --no-cache -U \
  npm \
  git \
  curl \
  build-base
RUN git clone https://github.com/Scetch/mcping.git 
WORKDIR /mcping
RUN cargo build --release

FROM alpine
COPY --from=builder /mcping/target/release/mcping-discord ./
CMD echo "token = \"$TOKEN\"" > config.toml  \
&& echo "address = \"$ADDRESS\"" >> config.toml \
&& echo "command = \"$COMMAND\"" >> config.toml \
&& ./mcping-discord

